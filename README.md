# GitLab Figma Plugin

> ⚠️ This plugin is in Beta.

The Official GitLab plugin to seamlessly upload your designs from Figma to GitLab issues (via [GitLab Design Management](https://docs.gitlab.com/ee/user/project/issues/design_management)).

## User Guide

For details on how to install and use the GitLab Figma Plugin, check out the [user guide](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/wikis/home).

## Developer guide

For details on how to set up your local development environment and contribute to the project, check out the [contribution guide](CONTRIBUTING.md)

## Documentation

See the [`docs/`](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/tree/master/docs) folder for project documentation:

- [Release process](docs/release-process.md)
- [Technical overview](docs/technical-overview.md)
