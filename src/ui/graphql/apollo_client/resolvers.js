import querystring from 'querystring';
import selectionQuery from '../queries/selection.query.graphql';
import userConfigQuery from '../queries/user_config.query.graphql';
import { toIssue, restApiUri } from '../../utils/gitlab_utils';
import { getUserConfigFromCache } from './utils';

export default {
  Mutation: {
    setSelection(_, { frames, components }, { cache }) {
      cache.writeQuery({
        query: selectionQuery,
        data: { selection: { __typename: 'Selection', frames, components } },
      });

      return { frames, components };
    },
    setUserConfig(_, { userConfig: { accessToken, gitlabInstance } }, { cache }) {
      const userConfig = {
        __typename: 'UserConfig',
        accessToken,
        gitlabInstance: {
          __typename: 'GitlabInstanceConfig',
          ...gitlabInstance,
        },
      };

      cache.writeQuery({
        query: userConfigQuery,
        data: {
          userConfig,
        },
      });

      return userConfig;
    },
  },
  Query: {
    async currentUser(_, __, { cache }) {
      const {
        accessToken,
        gitlabInstance: { host },
      } = getUserConfigFromCache(cache);
      if (!accessToken) throw new Error('Access token is not set.');
      if (!host) throw new Error('GitLab instance host is not set.');

      return fetch(restApiUri(host, '/user'), {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then(res => {
          if (res.status !== 200) {
            return res.json().then(({ message }) => {
              throw new Error(message);
            });
          }

          return res;
        })
        .then(res => res.json())
        .then(data => {
          const { id, name, username } = data;

          return {
            __typename: 'User',
            id,
            name,
            username,
          };
        });
    },
    async assignedIssues(_, __, { cache }) {
      const { accessToken, gitlabInstance } = getUserConfigFromCache(cache);
      if (!accessToken) throw new Error('Access token not set.');

      const query = {
        scope: 'assigned_to_me',
        order_by: 'updated_at',
        state: 'opened',
        per_page: 100,
        confidential: false,
      };
      return fetch(restApiUri(gitlabInstance.host, `/issues?${querystring.stringify(query)}`), {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then(res => {
          if (res.status !== 200) {
            return res.json().then(({ message }) => {
              throw new Error(message);
            });
          }

          return res;
        })
        .then(res => res.json())
        .then(data => {
          return data.map(issue => {
            return {
              ...toIssue(issue, { projectId: issue.project_id }),
              __typename: 'Issue',
            };
          });
        });
    },
  },
};
